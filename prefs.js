/* prefs.js
 *
 * Copyright (C) 2020 Martin Scherer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

const Gdk = imports.gi.Gdk;
const Gio = imports.gi.Gio;
const GObject = imports.gi.GObject;
const Gtk = imports.gi.Gtk;

const ExtensionUtils = imports.misc.extensionUtils;

var GameControllerSettings = GObject.registerClass(
class GameControllerSettings extends Gtk.ListBox {
    _init(params) {
        super._init(params);
        this.selection_mode = Gtk.SelectionMode.NONE;
        this.margin = 24;
        this._settings = ExtensionUtils.getSettings();
        this._blocked = [];

        this.add(this.make_row_switch('show-icon-no-controllers'));
    }

    make_row_switch(name, color) {
        let schema = this._settings.settings_schema;

        let row = new Gtk.ListBoxRow();

        let hbox = new Gtk.Box({
            orientation: Gtk.Orientation.HORIZONTAL,
            margin: 12,
        });

        row.add(hbox);

        let vbox = new Gtk.Box({orientation: Gtk.Orientation.VERTICAL});
        hbox.pack_start(vbox, true, true, 6);

        let sw = new Gtk.Switch({valign: Gtk.Align.CENTER});

        if (color) {
            let button = new Gtk.ColorButton({use_alpha: true});

            button.connect('notify::rgba', (widget, unused) => {
                let rgba = widget.get_rgba();
                let css = rgba.to_string();
                let idx = this._blocked.push(color);
                this._settings.set_string(color, css);
                this._blocked.splice(idx);
            });

            this._update_color_from_setting(button, color);

            this._settings.connect(`changed::${color}`, () => {
                this._update_color_from_setting(button, color);
            });

            hbox.pack_start(button, false, false, 6);
            sw.bind_property('active', button, 'sensitive',
                GObject.BindingFlags.SYNC_CREATE);

            let ckey = schema.get_key(color);
            button.set_tooltip_markup(ckey.get_description());
        }

        hbox.pack_start(sw, false, false, 0);

        let key = schema.get_key(name);

        let summary = new Gtk.Label({
            label: `<span size='medium'><b>${key.get_summary()}</b></span>`,
            hexpand: true,
            halign: Gtk.Align.START,
            use_markup: true,
        });

        vbox.pack_start(summary, false, false, 0);

        let description = new Gtk.Label({
            label: `<span size='small'>${key.get_description()}</span>`,
            hexpand: true,
            halign: Gtk.Align.START,
            use_markup: true,
        });
        description.get_style_context().add_class('dim-label');

        vbox.pack_start(description, false, false, 0);

        this._settings.bind(name, sw, 'active',
            Gio.SettingsBindFlags.DEFAULT);
        return row;
    }

    _update_color_from_setting(widget, name) {
        let idx = this._blocked.indexOf(name);
        if (idx !== -1)
            return;

        let str = this._settings.get_string(name);
        let rgba = new Gdk.RGBA();
        rgba.parse(str);
        widget.set_rgba(rgba);
    }
});

function init() {
    ExtensionUtils.initTranslations();
}

function buildPrefsWidget() {
    let widget = new GameControllerSettings();
    widget.show_all();

    return widget;
}
