#!/bin/sh
meson --prefix=$PWD --localedir=$PWD/locale --reconfigure .build
ninja -C .build install
glib-compile-schemas schemas
zip -r game-controllers@marsch84.zip ./*.js ./*.css ./*.json ./locale ./schemas
