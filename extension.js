/* extension.js
 *
 * Copyright (C) 2020 Martin Scherer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

const {
    Gio, GLib, GObject, GUdev, St,
} = imports.gi;

const ByteArray = imports.byteArray;
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = imports.misc.extensionUtils.getCurrentExtension();

const Gettext = imports.gettext.domain(Me.metadata['gettext-domain']);
const _ = Gettext.gettext;

let gameControllersMenu;

function onUEvent(client, action, device) {
    if (action === 'add') {
        gameControllersMenu.rebuildGameControllerItems();
    } else if (action === 'remove') {
        gameControllersMenu.gameControllerItems.forEach(gameControllerItem => {
            if (device.get_sysfs_path() === gameControllerItem.device.get_sysfs_path())
                gameControllersMenu.rebuildGameControllerItems();
        });
    }
}

let GameControllerMenuItem = GObject.registerClass(
class GameControllerMenuItem extends PopupMenu.PopupSubMenuMenuItem {
    _init(device) {
        this._device = device;
        this._calibrationData = {deviceName: '', correction: '', corectionDefault: '',
            mappings: '', mappingsDefault: ''};

        if (device) {
            let parent = device.get_parent();
            if (parent) {
                let property = parent.get_property('NAME');
                property = property.replace(/"/g, '');
                if (property)
                    this._calibrationData.deviceName = property;
                else
                    throw new Error('Parent of device has got not property "Name".');
            } else {
                throw new Error('Device has got no parent.');
            }
        } else {
            throw new Error('Device is null.');
        }

        let dataDir = GLib.get_user_data_dir();
        let destination = GLib.build_filenamev(
            [dataDir, 'game-controllers@marsch84', `${this._calibrationData.deviceName}.json`]);
        this._destinationFile = Gio.File.new_for_path(destination);

        if (GLib.mkdir_with_parents(this._destinationFile.get_parent().get_path(), 0o755))
            throw new Error('Directory for calibration data cannot be created.');

        let success = false;
        let contents;
        try {
            [success, contents] = this._destinationFile.load_contents(null);
        } catch (exception) {
            let [correction, mappings] = this._readCalibration();
            if (correction !== '') {
                this._calibrationData.correction = correction;
                this._calibrationData.correctionDefault = correction;
                this._calibrationData.mappings = mappings;
                this._calibrationData.mappingsDefault = mappings;
                if (!this._replaceCalibrationData())
                    throw new Error('Calibration data cannot be stored.');
            } else {
                throw new Error('Calibration data cannot be retrieved via "jscal".');
            }
        }
        if (success) {
            this._calibrationData = JSON.parse(ByteArray.toString(contents));
            if (!this._writeCalibration())
                throw new Error('Cannot apply stored calibration data.');
        }

        super._init(this._calibrationData.deviceName);

        this.menu.addAction(_('Save calibration'), () => this._onSaveCalibration());
        this.menu.addAction(_('Reset to default'), () => this._onResetToDefault());
    }

    get device() {
        return this._device;
    }

    _readCalibration() {
        let correction = this._getJscal('-p');
        let mappings = this._getJscal('-q');
        return [correction, mappings];
    }

    _getJscal(flag) {
        let output = '';
        let deviceFile = this._device.get_device_file();
        let argv = ['jscal', flag, deviceFile];

        if (deviceFile) {
            let [ok, stdout, unused, status] = GLib.spawn_sync(
                null, argv, null, GLib.SpawnFlags.SEARCH_PATH, null);
            if (ok && !status)
                output = ByteArray.toString(stdout);
        }
        return output;
    }

    _writeCalibration() {
        let okCorrecton = this._setJscal(this._calibrationData.correction);
        let okMappings = this._setJscal(this._calibrationData.mappings);
        return okCorrecton & okMappings;
    }

    _setJscal(calibrationData) {
        let success = false;
        let argv = calibrationData.split(' ');
        argv[3] = this._device.get_device_file();

        if (argv[3]) {
            let [ok, unused1, unused2, status] = GLib.spawn_sync(
                null, argv, null, GLib.SpawnFlags.SEARCH_PATH, null);
            if (ok && !status)
                success = true;
        }
        return success;
    }

    _replaceCalibrationData() {
        let dataJSON = JSON.stringify(this._calibrationData);
        let [success, unused] = this._destinationFile.replace_contents(
            dataJSON, null, false, Gio.FileCreateFlags.REPLACE_DESTINATION, null);
        return success;
    }

    _onSaveCalibration() {
        let [correction, mappings] = this._readCalibration();
        if (correction !== '' && mappings !== '') {
            this._calibrationData.correction = correction;
            this._calibrationData.mappings = mappings;
            if (!this._replaceCalibrationData())
                Main.notifyError(_('Calibration data cannot be stored.'));
        } else {
            Main.notifyError(_('Calibration data cannot be retrieved via "jscal".'));
        }
    }

    _onResetToDefault() {
        this._calibrationData.correction = this._calibrationData.correctionDefault;
        this._calibrationData.mappings = this._calibrationData.mappingsDefault;
        if (!this._replaceCalibrationData())
            Main.notifyError(_('Calibration data cannot be stored.'));
        if (!this._writeCalibration())
            Main.notifyError(_('Cannot apply stored calibration data.'));
    }
});

let GameControllersMenuButton = GObject.registerClass(
class GameControllersMenuButton extends PanelMenu.Button {
    _init() {
        super._init(0, 'GameControllersMenuButton', false);
        this._settings = ExtensionUtils.getSettings();

        this._client = new GUdev.Client({subsystems: ['input']});
        this._client.connect('uevent', onUEvent);

        this._signals = [];
        this._connect(this._settings,
            'changed::show-icon-no-controllers',
            this._sync.bind(this));

        let gameControllersIcon = new St.Icon({
            icon_name: 'input-gaming-symbolic',
            style_class: 'system-status-icon',
        });

        let topBox = new St.BoxLayout();
        topBox.add_actor(gameControllersIcon);
        this.add_actor(topBox);

        this._gameControllerItems = [];
        this.rebuildGameControllerItems();
    }

    rebuildGameControllerItems() {
        let gameDevices = this._getGameDevices();
        this.menu.removeAll();
        this._gameControllerItems = [];

        let ok = false;
        try {
            ok = GLib.spawn_sync(null, ['jscal'], null, GLib.SpawnFlags.SEARCH_PATH, null);
        } catch (error) {
            log(`Error in calling "jscal": ${error.message}`);
        }
        if (!ok) {
            this.menu.addMenuItem(new PopupMenu.PopupMenuItem(
                _('Install utility "jscal" (e.g. in package "joystick")'), {reactive: false}));
        } else {
            for (let i = 0; i < gameDevices.length; ++i) {
                let itemGameController = null;

                try {
                    itemGameController = new GameControllerMenuItem(gameDevices[i]);
                } catch (error) {
                    log(`Error in adding game device to menu: ${error.message}`);
                    continue;
                }
                this._gameControllerItems.push(itemGameController);
                this.menu.addMenuItem(itemGameController);
            }
            if (!this._gameControllerItems.length) {
                this.menu.addMenuItem(new PopupMenu.PopupMenuItem(
                    _('No game controller detected'), {reactive: false}));
            }
        }

        ok = false;
        try {
            ok = GLib.spawn_sync(
                null, ['jstest-gtk', '-v'], null, GLib.SpawnFlags.SEARCH_PATH, null);
        } catch (error) {
            log(`Error in calling "jstest-gtk": ${error.message}`);
        }
        if (ok) {
            this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
            this.menu.addAction(
                _('Calibrate game controllers (jstest-gtk)'), () => this._onLaunchJstestGtk());
        }
        this._sync();
    }

    _getGameDevices() {
        let devices = [];

        for (let i = 0; i < 32; ++i) {
            let device = this._client.query_by_subsystem_and_name('input', `js${i}`);
            if (device)
                devices.push(device);
        }
        return devices;
    }

    _onLaunchJstestGtk() {
        let ok = GLib.spawn_async(null, ['jstest-gtk'], null,
            GLib.SpawnFlags.SEARCH_PATH | GLib.SpawnFlags.DO_NOT_REAP_CHILD, null);
        if (!ok)
            Main.notifyError(_('Unable to launch "jstest-gtk".'));
    }

    _connect(obj, signal, handler) {
        let id = obj.connect(signal, handler);
        this._signals.push([obj, id]);
    }

    _sync() {
        this.visible = this._gameControllerItems.length ||
            this._settings.get_boolean('show-icon-no-controllers');
    }

    get gameControllerItems() {
        return this._gameControllerItems;
    }
});

class Extension {
    enable() {
        gameControllersMenu = new GameControllersMenuButton();
        Main.panel.addToStatusArea('gameControllersMenu', gameControllersMenu);
    }

    disable() {
        gameControllersMenu.destroy();
    }
}

function init() {
    ExtensionUtils.initTranslations();
    return new Extension();
}
