# Gnome Shell Extension "Game Controllers"
## About
This extension provides the possibility to store game controller calibration settings. It loads the stored settings at start-up. The extension is basically a wrapper for the applications "jscal" and "jstest-gtk". It adds the load and store functionality that is not available through these utilities by default. The extension uses the classical joystick interface of linux, as this is still the standard in most games. It does not support the more recent event device interface.

The extension requires JavaScript bindings for the GUdev library which is not installed by default on some distributions. Run the command below to install the necessary bindings for distributions based on Ubuntu.

Please install the applications "jscal" (e.g. in package "joystick" for Ubuntu) and "jstest-gtk" for full functionality. These utilities are part of most distributions and can be installed via the respective package management system.

![Screenshot](https://gitlab.gnome.org/marsch84/game-controllers/-/raw/master/data/game-controllers.png)

## Installation
Install required packages (on Ubuntu):
```bash
sudo apt install joystick jstest-gtk gir1.2-gudev-1.0
```
Install extension:
```bash
./create-zip.sh
gnome-extensions install game-controllers@marsch84.zip
```

## License
[GPLv3](http://www.gnu.org/licenses/gpl-3.0.en.html)
