# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-27 21:00+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: extension.js:104
msgid "Save calibration"
msgstr ""

#: extension.js:105
msgid "Reset to default"
msgstr ""

#: extension.js:165 extension.js:175
msgid "Calibration data cannot be stored."
msgstr ""

#: extension.js:167
msgid "Calibration data cannot be retrieved via \"jscal\"."
msgstr ""

#: extension.js:177
msgid "Cannot apply stored calibration data."
msgstr ""

#: extension.js:216
msgid "Install utility \"jscal\" (e.g. in package \"joystick\")"
msgstr ""

#: extension.js:232
msgid "No game controller detected"
msgstr ""

#: extension.js:240
msgid "Calibrate game controllers (jstest-gtk)"
msgstr ""

#: extension.js:260
msgid "Unable to launch \"jstest-gtk\"."
msgstr ""

#: schemas/org.gnome.shell.extensions.game-controllers.gschema.xml:6
msgid "Show icon with no controllers connected"
msgstr ""

#: schemas/org.gnome.shell.extensions.game-controllers.gschema.xml:7
msgid "Show status bar icon when no game controllers are connected."
msgstr ""
